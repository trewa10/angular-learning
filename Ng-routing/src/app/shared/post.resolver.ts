import { PostsService } from './../posts.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Post } from "../posts.service";
import { Observable, delay, from } from "rxjs";
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class PostResolver implements Resolve<Post> {

  constructor(private postsService: PostsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Post | Observable<Post> | Promise<Post> {
    return from([this.postsService.getById(+route.params['id'])])
      .pipe(delay(1500))
  }

}