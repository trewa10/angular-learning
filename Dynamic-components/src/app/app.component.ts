import {Component, ComponentFactoryResolver, ViewChild} from '@angular/core'
import { ModalComponent } from './modal/modal.component'
import { RefDirective } from './modal/ref.directive';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @ViewChild(RefDirective)
  refDir!: RefDirective;

  constructor(
    private resolver: ComponentFactoryResolver,                                          // deprecated since v 13
    private title: Title,
    private meta: Meta
  ) {
    title.setTitle('App Component Page');
    meta.addTags([
      {name: 'keywords', content: 'angular, google, appcomponent'},
      {name: 'description', content: 'this is app component'}
    ])
  }

  showModal() {
    // const modalFactory = this.resolver.resolveComponentFactory(ModalComponent);       // deprecated since v 13
    // this.refDir.containerRef.createComponent(modalFactory);                           // 
    
    this.refDir.containerRef.clear();
    const component = this.refDir.containerRef.createComponent(ModalComponent);

    component.instance.title = 'Modal window';
    component.instance.close.subscribe(() => {
      this.refDir.containerRef.clear();
    })
    
  }
}

